#job.py
from uuid import uuid4
import redis
import json
from hotqueue import HotQueue

def generate_jid():
        return str(uuid.uuid4())

def generate_job_key(jid):
        return 'job.{}'.format(jid)

rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=1)
q = HotQueue("queue", host='172.17.0.1', port=6379, db=0)


def _instantiate_job(jid,status,start,end):
        if type(jid) == str:
                return {'id':jid, 'status':status, 'start':start, 'end':end}
#        else:
#                return {'id': jid.decode('utf-8'), 'status': status.decode('utf-8'), 'start': start.decode('utf-8'), 'end': end.decode('utf-8')}
        return 2

def _save_job(job_key, job_dict):	
        rd.hmset(job_key, job_dict)

def queue_job(jid):
        q.put(jid)

def add_job(start, end, status="submitted"):
        jid = generate_jid()
        job_dict = _instantiate_job(jid, status, start, end)
        save_job(generate_job_key(jid),job_dict)
        queue_job(jid)
        return job_dict

def update_job_status(jid, status):
        jid, status, start, end = rd.hmget(generate_job_key(jid), 'id', 'status', 'start', 'end')
        job = _instantiate_job(jid, status, start, end)
        if job:
                job['status'] = status
                save_job(generate_job_key(jid), job)
        else:
                raise Exception()
