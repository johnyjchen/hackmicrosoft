#api.py
from hotqueue import HotQueue
import jobs
import json
from flask import Flask, jsonify, request

# The main Flask app
api = Flask(__name__)


#if __name__ == '__main__':
#	app.run(debug=True, host='0.0.0.0')

q = HotQueue("queue", host='172.17.0.1', port=6379, db=0)

@api.route('/test')
def test():
    q.put(5)
    return json.dumps(5)

@api.route('/test2')
def test2():
    return json.dumps(len(q))

@api.route('/test3')
def test3():
    return q.get()

@api.route('/jobs', methods=['POST'])
def jobs_api():
	try:
		job = request.get_json(force=True)
	except Exception as e:
		return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
	return json.dumps(jobs.add_job(job['start'], job['end']))
