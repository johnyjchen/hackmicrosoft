
from api import api
from requests import get, post

if __name__ == "__main__":
    api.run(host='0.0.0.0', port=5000)
